# Clean Up Spam Issues in GitLab Projects

A python script using the GitLab API to clean up spam issues from projects.

Note: This script will delete things that might not be recoverable! **USE AT YOUR OWN RISK!**

## How to Use

When run the script will clean up (delete or close) issues from known spam users. It also collects spam issue titles to "learn" for future runs. When running as a CI/CD job a each issue will be backed up as a `.json` file before deletion and stored in the job artifacts. 

### Using Your Own Runner (Recommended)

A little more complicated but recommended as you have the best control over what heappens and issue back ups do not leave your server.

1. Fork this project
2. In the fork add a CI/CD variable `GITLAB_ACCESS_TOKEN` with an access token of your user that has at least API read/write access
  - The script will **close** issues if you are a `Maintainer`
  - The script will **delete** issues if you are an `Owner`
3. Run the pipeline (either through schedule or by pushing the main branch)
4. (optional) Occasionally accept MRs created by the bot to enhance spam detection
5. (optional) Occasionally create an MR to https://git.rwth-aachen.de/bots/unspam to allow everyone to enhance their spam detection

### With @rwthbot

Less complicated but your issues will be processed centrally by the bot user.

1. Add @rwthbot as owner (for deletion) or as maintainer (for closing).

@rwthbot will regularly visit your project and remove spam. Backups will be in the project https://git.rwth-aachen.de/bots/unspam and may be visible to others.

## How This Works

The script uses gitlab-python to access the GitLab API. The script is designed to run as a CI/CD task in GitLab Runner.

Configuration is done through environment variables:
- `CI_SERVER_URL`: GitLab server URL (usually set by default in GitLab Runner)
- `CI_PROJECT_ID`: Project ID of this project (usually set by default in GitLab Runner)
- `GITLAB_ACCESS_TOKEN`: A users' access token to access the projects (at least `maintainer` role and `api` permissions)

The script enumerates all projects with recent activities and all recently updates issues.

The issue authors are checked against the list in `spam_users.txt`. If the issue is by a spam user, the issue is deleted. And the title is added to the titles list.

The issue titles are checked agains the list in `spam_titles.txt`. If the issue title is in the list, the user is added as a spammer.

Both titles and users list are updated and submitted as a MR to the CI project. The MR can be reviewed and merged to update the spam detection.

See [main.py](main.py) for details.

## Authors

Marius Politze, @mpolitze, [0000-0003-3175-0659](https://orcid.org/my-orcid?orcid=0000-0003-3175-0659)