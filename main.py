import gitlab
import os
import datetime
import json

def log(s):
    print(s)

gitServerUrl = os.environ['CI_SERVER_URL']
gl = gitlab.Gitlab(url=gitServerUrl, private_token=os.environ['GITLAB_ACCESS_TOKEN'])
ci_project = gl.projects.get(os.environ['CI_PROJECT_ID'])
gl.auth()

log(f'Hello I am {gl.user.username}.')

log(f'Starting at {datetime.datetime.now()}.')

spam_users = []
new_spam_users = {}
log('Loading spam users from `spam_users.txt`...')
if os.path.exists('spam_users.txt'):
    f = open('spam_users.txt')
    for l in f:
        spam_users.append(l.strip())    
else:
    log('  file not found fallback to fixed value,')
    spam_users = ['ghost', 'leadohlead', 'nhjoshua58', 'williamjvmarlow']
log(f'  loaded {len(spam_users)} spam users.')

log('Checking spam user status...')
for u in spam_users:
    [user] = gl.users.list(username=u) or [None]
    if user :
        log(f'  user {user.username} (id: {user.id}) does not seem to be blocked but is in list.')
        log(f'  go to {gitServerUrl}/{user.username} to report.')

spam_titles = []
new_spam_titles = {}
log('Loading spam titles from `spam_titles.txt`...')
if os.path.exists('spam_titles.txt'):
    f = open('spam_titles.txt')
    for l in f:
        spam_titles.append(l.strip())
log(f'  loaded {len(spam_titles)} spam titles.')

log('Checking for spam issues in projects...')
issueBuckets = {}
maxIssueAge = datetime.timedelta(days=5)
projects = gl.projects.list(last_activity_after=datetime.datetime.now()-maxIssueAge, min_access_level=40, get_all=True)
log(f'  {len(projects)} have recent activity.')
for p in projects :
    log(f'  checking project `{p.path_with_namespace}`.')
    issues = p.issues.list(updated_after=datetime.datetime.now()-maxIssueAge, get_all=True)
    log(f'    project has {len(issues)} recent issues.')
    for i in issues:
        issueBucketId = f'{i.created_at[:13]}_{i.author["username"]}'
        issueBuckets.setdefault(issueBucketId, 0)
        issueBuckets[issueBucketId] += 1

        if i.author['username'] in spam_users:
            log(f'    issue {i.web_url} identified as spam due to author. Taking action.')
            new_spam_titles[i.title.strip()] = 1
            backPath = f'./backup/{i.references["full"]}'.replace('#','_')
            log(f'      writing backup to {backPath}.')
            os.makedirs(backPath, exist_ok=True)
            with open(f'{backPath}/issue.json', 'w') as f:
                f.write(i.to_json())
            try:                
                log(f'      deleting issue.')
                i.delete()
            except Exception as e:
                log(f'      error deleting issue: `{e}`. Closing instead.')
                try:
                    pass
                    i.state_event = 'close'
                    i.save()
                except Exception as e:
                    log(f'    error closing issue: `{e}`. Skipping.')
        elif i.title in spam_titles:
            log(f'    issue {i.web_url} identified as spam due to title.')
            new_spam_users[i.author['username']] = 1

log(f'Checking for users with abnormal activity.')
for k,v in issueBuckets.items():
    if v > 9:
        author = k[14:]
        time = k[:13]
        log(f'  `{author}` created {v} issues at {time}, identified as spam due to frequency.')
        new_spam_users[author] = 1

log('Saving spam telemetry...')
for t in spam_titles:
    new_spam_titles.pop(t.strip(), 1)

for u in spam_users:
    new_spam_users.pop(u.strip(), 1)

log(f'  found {len(new_spam_titles)} new titles and {len(new_spam_users)} new users.')

if len(new_spam_titles) or len(new_spam_users):
    commitData = {
        'branch': 'autoupdate',
        'commit_message': f'Update after job {os.environ["CI_JOB_ID"]}',
        'actions': []
    }

    if len(new_spam_titles):
        log(f'    add new titles to commit.')
        commitData['actions'].append({
            'action': 'update',
            'file_path': 'spam_titles.txt',
            'content': '\n'.join(spam_titles + list(new_spam_titles.keys()))
        })

    if len(new_spam_users):
        log(f'    add new users to commit.')
        commitData['actions'].append({
            'action': 'update',
            'file_path': 'spam_users.txt',
            'content': '\n'.join(spam_users + list(new_spam_users.keys()))
        })

    branchName = 'autoupdate'
    [b] = ci_project.branches.list(search=f'^{branchName}$') or [None]
    if not b:
        log(f'  will create new branch `{branchName}`.')
        ci_project.branches.create({
            'branch': branchName,
            'ref': os.environ['CI_COMMIT_SHA']
        })

    log(f'  will commit {len(commitData["actions"])} files.')  
    ci_project.commits.create(commitData)

    [m] = ci_project.mergerequests.list(source_branch=branchName, state='opened') or [None]
    if not m:
        log('  creating new merge request.')
        m = ci_project.mergerequests.create({
            'source_branch': branchName,
            'target_branch': ci_project.default_branch,
            'title': 'Autoupdate'
        })

    log(f'  merge {m.web_url} to update.')


log(f'Done at {datetime.datetime.now()}.')
